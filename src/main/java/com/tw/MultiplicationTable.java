package com.tw;

import java.util.StringJoiner;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if (!isValid(start, end)){
            return null;
        }
        return generateTable(start, end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return number <= 1000 && number >= 1;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        StringJoiner allLine = new StringJoiner("\n");
        for (int i = 0; i <= end - start; i++) {
            String line = generateLine(start, start + i);
            allLine.add(line);
        }
        return allLine.toString();
    }

    public String generateLine(int start, int row) {
        StringJoiner line = new StringJoiner("  ");
        for (int i = 0; i < row-1; i++) {
            String expression = generateSingleExpression(start+i, row);
            line.add(expression);
        }
        return line.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        String expression = multiplicand+"*"+multiplier+"="+multiplicand*multiplier;
        return expression;
    }
}
